Name: fairsoft-release
Version: 0
Release: 3.dev%{?_dist}
Summary: Extra Packages for the FairSoft distribution
License: LGPLv3
Source0: RPM-GPG-KEY-fairsoft
Source1: fairsoft.repo
BuildArch: noarch
Requires: fedora-release

%description
This package contains the Extra Packages for the FairSoft distribution
GPG key as well as configuration for dnf.

%prep

%build

%install
install -p -m 644 -D %{SOURCE0} \
  %{buildroot}%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-fairsoft-%{fedora}-%{_arch}
install -p -m 644 -D -t %{buildroot}%{_sysconfdir}/yum.repos.d %{SOURCE1}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/yum.repos.d/*
%{_sysconfdir}/pki/rpm-gpg/*

%changelog
* Sat Apr 22 2023 Dennis Klein <d.klein@gsi.de> - 0-3.dev
- Renew expired key
* Wed Jun  8 2022 Dennis Klein <d.klein@gsi.de> - 0-2.dev
- Disable source repo by default
- Include debuginfo repo
- Do not version-pin dependency to fedora-release
* Sun May  2 2021 Dennis Klein <d.klein@gsi.de> - 0-1.dev
- Initial release
